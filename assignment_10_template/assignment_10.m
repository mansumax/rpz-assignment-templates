% Assignment 10: K-means

%% Init
%run('../../stprtool/stprpath.m')
rng(0); % Initialization of seed for random number generation

%% Load data
load image_data.mat % Images of letters
load wine.mat % Dataset with wine measurements
im = imread('geeks.png');

%% K-means
fprintf('K-means...\n');
x = compute_measurements(images);
[c, means, x_dists] = k_means(x, 3, Inf, true);
show_clusters(x, c, means);

%% Avoid Local Minima
fprintf('K-means with multiple trials...\n');
[c_m, means_m, x_dists_m] = k_means_multiple_trials(x, 3, 10, Inf, true);
show_clusters(x, c_m, means_m);

%% Estimate Optimal Number of Clusters
elbow_graph(wine, 1:10, 10, Inf, false);
pause;

%% K-means Application - Color Quantization
im_q = quantize_colors(im, 8);
figure;
imshow(im_q);
