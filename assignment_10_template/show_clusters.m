function show_clusters(x, c, means)
% show_clusters(x, c, means)
% Input:
%   x         .. Feature vectors, of size [dim, number_of_vectors].
%                This function supports only vectors of dim = 2.
%
%   c         .. Cluster index for each feature vector, of size
%                [1, number_of_vectors].
%
%   means     .. Cluster centers, of size [dim, k], i.e. means(:,i) is the
%                center of the i-th cluster.

if size(x, 1) == 2
    figure(1);
    cla;
    ppatterns(x, c);
    plot(means(1,:), means(2,:), '+m', 'markersize', 12, 'linewidth', 3);
    grid on;
    pause;
end
