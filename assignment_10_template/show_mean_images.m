function show_mean_images(images, c)
% show_avarage_images(images, c)
%
% Shows mean images of each image cluster.
%
% Input:
%   images    .. Input images, of size [10,10,number_of_images].
%
%   c         .. Cluster index for each feature vector, of size
%                [1,number_of_images], containing only values from 1 to k,
%                i.e. c(i) is the index of a cluster which the image
%                images(:,:,i) belongs to.

labels = unique(c);
n_classes = numel(labels);

figure(2);
for i = 1:n_classes
    mean_image = mean(images(:, :, c==labels(i)), 3);
    
    % Visualization
    subplot(1, n_classes, i);
    imshow(uint8(mean_image));
    title(sprintf('Mean of class %d', labels(i)));
end
